import datetime
import requests
import time
import pandas
class GDAX(object):
  def __init__(self, pair):
    self.pair = pair
    self.uri = 'https://api.gdax.com/products/{pair}/candles'.format(pair=self.pair)


  def fetch(self, start, end, granularity):
    data = []
    # We will fetch the candle data in windows of maximum 100 items.
    delta = datetime.timedelta(minutes=granularity * 100)

    slice_start = start
    while slice_start != end:
      slice_end = min(slice_start + delta, end)
      data += self.request_slice(slice_start, slice_end, granularity)
      slice_start = slice_end

    # I prefer working with some sort of a structured data, instead of
    # plain arrays.
    data_frame = pandas.DataFrame(data=data, columns=['time', 'low', 'high', 'open', 'close', 'volume'])
    data_frame.set_index('time', inplace=True)
    return data_frame

  def request_slice(self, start, end, granularity):
    # Allow 3 retries (we might get rate limited).
    retries = 3
    for retry_count in range(0, retries):
      # From https://docs.gdax.com/#get-historic-rates the response is in the format:
      # [[time, low, high, open, close, volume], ...]
      response = requests.get(self.uri, {
        'start': GDAX.__date_to_iso8601(start),
        'end': GDAX.__date_to_iso8601(end),
        'granularity': granularity * 60  # GDAX API granularity is in seconds.
      })

      if response.status_code != 200 or not len(response.json()):
        if retry_count + 1 == retries:
          raise Exception('Failed to get exchange data for ({}, {})!'.format(start, end))
        else:
          # Exponential back-off.
          time.sleep(1.5 ** retry_count)
      else:
        # Sort the historic rates (in ascending order) based on the timestamp.
        result = sorted(response.json(), key=lambda x: x[0])
        return result

  @staticmethod
  def __date_to_iso8601(date):
      return '{year}-{month:02d}-{day:02d}T{hour:02d}:{minute:02d}:{second:02d}'.format(
          year=date.year,
          month=date.month,
          day=date.day,
          hour=date.hour,
          minute=date.minute,
          second=date.second)

profit_dict = {}
data_frame = GDAX('ETH-USD').fetch(datetime.datetime(2018, 2, 1), datetime.datetime(2018, 7, 1), 360)
print(data_frame)
limit_sell = 5
stop_loss = -5
both = 0
total_stop = 0
total_limit = 0
while (limit_sell < 50):
    limit_sell = limit_sell + 1
    stop_loss = -5
    while(stop_loss > -50):
        stop_loss = stop_loss - 1
        newday_index = data_frame.index.min()
        current_index = newday_index
        last_index = data_frame.index.max()
        profit = 0
        sold_limit = 0
        sold_stop = 0
        hour = 0
        buy_price = data_frame.get_value(newday_index, 'open')
    #    print("first Price", buy_price)
        while current_index < last_index:
            if hour == 24:
                newday_index = newday_index + hour*3600
                if newday_index > last_index:
                    break
                hour = 0
            if sold_limit or sold_stop:
                sold_limit = 0
                sold_stop = 0
                buy_price = data_frame.get_value(newday_index, 'open')
    #            print("buy price", buy_price)
            current_index = newday_index + hour*3600
            hour = hour + 6
            if (data_frame.get_value(current_index, 'high') > (buy_price + limit_sell)):
                sold_limit = 1
                total_limit += 1
                profit = profit + limit_sell
                hour = 24
            if (data_frame.get_value(current_index, 'low') < (buy_price + stop_loss)):
        #        print("sold for a loss!", data_frame.get_value(current_index, 'low'))
                sold_stop = 1
                total_stop += 1
                profit = profit + stop_loss
                hour = 24
            if (sold_limit & sold_stop):
                # print("--both--")
                both = both + 1
        #profit_dict[str([profit, limit_sell, stop_loss])] = profit
        profit_dict[str([profit, limit_sell, stop_loss])] = profit
#        print("---%d ---------------%d----------", profit, limit_sell, stop_loss)
#print(data_frame.get_value(1530403200, 'low'))
# print(profit_dict)

# for value in sorted(profit_dict.keys()):
#     print("%s: %s" % (value, value))
sorted_dict = sorted(profit_dict.keys(), key=profit_dict.__getitem__)
print("\n".join(sorted_dict))
print("\nLimit:", total_limit)
print("\nStop:", total_stop)
print("\nBoth:", both)
print("Total: ", (total_limit +total_stop + both))
# keylist = profit_dict.keys()
# keylist.sort()
# for key in keylist:
#     print "%s: %s" % (key, mydict[key])


#data_frame.to_csv('feb1min.csv')
