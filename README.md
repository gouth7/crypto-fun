IMAGINE you buy 1 Ether at midnight every day.
You set a limit sell and a stop loss, and fill whichever is reached first.

This script tests this strategy for any specified time period with every combination of limit sell and stop loss prices within $40 of the starting price at midnight.

The output is a three element list [profit during time period, difference between limit sell and starting price, difference between stop loss and starting price]
The outputs are organized in order of most profitable to least. 
